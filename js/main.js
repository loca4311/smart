for (let i = 1; i <= 6; i++) {
    let galleryThumbs = new Swiper(`.gallery-thumbs${i}`, {
        spaceBetween: 10,
        slidesPerView: 5.2,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            300: {
                slidesPerView: 3.2,
                spaceBetween: 4,
            },
            635: {
                slidesPerView: 4.2,
                spaceBetween: 7,
            },
            1024: {
                slidesPerView: 5.2,
                spaceBetween: 7,
            },

        }
    });

    new Swiper(`.gallery-top${i}`, {
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });
}

const mybutton = document.getElementById("myBtn");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
